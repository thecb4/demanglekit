# DemangleKit Change Log
All notable changes to this project will be documented in this file.

* Format based on [Keep A Change Log](https://keepachangelog.com/en/1.0.0/)
* This project adheres to [Semantic Versioning](http://semver.org/).

## [0.2.3] - 2019-MAY-09 (50).
### Added
-

### Changed
-

### Deprecated
-

### Removed
-

### Fixed
-

### Security
-


## [0.2.2] - 2019-MAY-09 (48).
### Added
- Swift Language Versions support v4.2, v5 in Package.swift

### Changed
-

### Deprecated
-

### Removed
-

### Fixed
- sources in Package.swift file for gitlab CI jobs
- gitlab-ci job names in pages stage

### Security
-


## [0.2.0] - 2019-MAY-09 (46).
### Added
- carthage support (iOS tvOS macOS)
- xcodegen dependency

### Changed
-

### Deprecated
-

### Removed
-

### Fixed
-

### Security
-


## [0.1.1] - 2019-MAY-06 (35).
### Added
- Docs link in README

### Changed
- docs to pages in gitlab-ci.yml

### Deprecated
-

### Removed
-

### Fixed
- pages generation (see docs to pages change)

### Security
-


## [0.1.0] - 2019-MAY-06 (33).
### Added
- Demangle.swift from https://github.com/apple/swift/blob/master/lib/Demangling/Demangler.cpp
- DemangleKitTests from https://github.com/apple/swift/blob/master/lib/Demangling/Demangler.cpp

### Changed
-

### Deprecated
-

### Removed
-

### Fixed
-

### Security
-
