#!/usr/bin/swift sh

import Foundation
import Shelltr // https://gitlab.com/thecb4/Shelltr == 0.3.0
import Path // mxcl/Path.swift == 0.16.2
import Version // @mxcl == 1.1.0
// import ChangeLogger // https://gitlab.com/thecb4/changelogger == 0.1.0

extension Version {
  public class BuildDateFormatter: DateFormatter {
    public static var defaultFormat = "yyyyMMMdd'T'HH:mm:ss"
    public static var defaultTimeZone = "UTC"

    public init(format: String = BuildDateFormatter.defaultFormat, tzAbbreviation: String = BuildDateFormatter.defaultTimeZone) {
      super.init()
      dateFormat = format
      timeZone = TimeZone(abbreviation: tzAbbreviation)
    }

    public required init?(coder aDecoder: NSCoder) {
      super.init(coder: aDecoder)
    }
  }

  public static var path = Path.cwd / ""
  public static var buildtimeStampFormatter = Version.BuildDateFormatter()

  public enum Error: Swift.Error {
    case couldNotLoad(String)
    case malformedBuildMetadata([String])
    case failedToCreateNewFormattedBuildTimeStamp
  }

  public enum Element {
    case major
    case minor
    case patch
  }

  public var build: Int? {
    return Int(String(buildMetadataIdentifiers[0]))
  }

  // https://stackoverflow.com/questions/35700281/date-format-in-swift
  public var buildTimestamp: Date? {
    // let dateFormatter = BuildDateFormatter()
    // dateFormatter.dateFormat = Version.buildtimeStampFormat.format
    // dateFormatter.timeZone = TimeZone(abbreviation: Version.buildtimeStampFormat.tzAbbreviation)
    let buildTimestamp: Date? = Version.buildtimeStampFormatter.date(from: buildMetadataIdentifiers[1])
    return buildTimestamp
  }

  public func newBuildTimestampString(using formatter: DateFormatter = Version.buildtimeStampFormatter) throws -> String {
    // let dateFormatter = DateFormatter()
    // dateFormatter.dateFormat = Version.buildtimeStampFormat.format
    // dateFormatter.timeZone = TimeZone(abbreviation: Version.buildtimeStampFormat.tzAbbreviation)
    let buildTimestampString = formatter.string(from: Date())
    return buildTimestampString
  }

  public static func load(from path: Path = Version.path) throws -> Version? {
    let source = try String(contentsOf: path)
    return Version(source.trimmingCharacters(in: .whitespacesAndNewlines))
  }

  public func save(to path: Path) throws {
    try description.write(to: path)
  }

  public func bump(_ element: Version.Element) throws -> Version {
    var buildMetadataIdentifiers = self.buildMetadataIdentifiers

    guard let buildTimestamp = try Version.load()?.newBuildTimestampString() else {
      throw Version.Error.failedToCreateNewFormattedBuildTimeStamp
    }

    buildMetadataIdentifiers[1] = buildTimestamp

    switch element {
      case .major:
        return Version(
          major + 1,
          0,
          0,
          prereleaseIdentifiers: prereleaseIdentifiers,
          buildMetadataIdentifiers: buildMetadataIdentifiers
        )
      case .minor:
        return Version(
          major,
          minor + 1,
          0,
          prereleaseIdentifiers: prereleaseIdentifiers,
          buildMetadataIdentifiers: buildMetadataIdentifiers
        )
      case .patch:
        return Version(
          major,
          minor,
          patch + 1,
          prereleaseIdentifiers: prereleaseIdentifiers,
          buildMetadataIdentifiers: buildMetadataIdentifiers
        )
    }
  }

  // build metadata Format
  // buildNumber.datetime
  public func bumpBuild() throws -> Version {
    guard let buildNumber = self.build else {
      throw Version.Error.malformedBuildMetadata(self.buildMetadataIdentifiers)
    }

    var buildMetadataIdentifiers = self.buildMetadataIdentifiers

    buildMetadataIdentifiers[0] = String(describing: buildNumber + 1)

    guard let buildTimestamp = try Version.load()?.newBuildTimestampString() else {
      throw Version.Error.failedToCreateNewFormattedBuildTimeStamp
    }

    buildMetadataIdentifiers[1] = buildTimestamp

    return Version(
      major,
      minor,
      patch,
      prereleaseIdentifiers: prereleaseIdentifiers,
      buildMetadataIdentifiers: buildMetadataIdentifiers
    )
  }

  public static func bumpBuild(in path: Path = Version.path) throws {
    guard let version = try Version.load(from: path) else {
      throw Version.Error.couldNotLoad(path.string)
    }
    try version.bumpBuild().save(to: path)
  }

  public static func bump(_ element: Version.Element, in path: Path = Version.path) throws {
    guard let version = try Version.load(from: path) else {
      throw Version.Error.couldNotLoad(path.string)
    }
    try version.bump(element).save(to: path)
  }
}

if #available(macOS 10.13, *) {
  Shell.work {
    Version.path = Path.cwd / "version.md"

    try Version.bumpBuild()

    // tell me what you are changing
    try Shell.assertModified(file: "CHANGELOG.md")

    // // get rid of all the old data
    try Shell.rm(content: "Demangle.xcodeproj", recursive: true, force: true)
    try Shell.rm(content: ".build", recursive: true, force: true)
    try Shell.rm(content: "DerivedData", recursive: true, force: true)

    // Format & Lint
    try Shell.swiftformat()
    try Shell.swiftlint(quiet: false)

    // Test SPM macOS
    try Shell.swifttest(arguments: ["--enable-code-coverage"])
    try Shell.copy(source: ".build/debug/codecov/DemangleKit.json", destination: "Tests/coverage/Coverage.json")

    // Test xcodebuild
    try Shell.xcodegenGenerate()
    try Shell.xcodebuild(arguments: [
      "test -project Demangle.xcodeproj -scheme DemangleKit-iOS -destination 'platform=iOS Simulator,name=iPhone X'"
    ])
    try Shell.xcodebuild(arguments: [
      "test -project Demangle.xcodeproj -scheme DemangleKit-tvOS -destination 'platform=tvOS Simulator,name=Apple TV'"
    ])
    try Shell.xcodebuild(arguments: [
      "test -project Demangle.xcodeproj -scheme DemangleKit-macOS -destination 'platform=macOS'"
    ])

    // carthage package
    try Shell(.bash).execute("/bin/bash", arguments: [
      "-c",
      "carthage build --no-skip-current --platform iOS tvOS macOS"
    ])
    try Shell(.bash).execute("/bin/bash", arguments: [
      "-c",
      "carthage archive DemangleKit"
    ])

    try Shell.sourcekitten(module: "DemangleKit", destination: Shell.cwd + "/docs/source.json")
    try Shell.jazzy()

    // add + commit
    try Shell.gitAdd(.all)
    try Shell.gitCommit(message: "Fixed pages generation")
  }
}
