import XCTest

import DemangleKitTests

var tests = [XCTestCaseEntry]()
tests += DemangleKitTests.__allTests()

XCTMain(tests)
