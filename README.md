# DemangleKit

Swift Package of [CwlDemange](https://github.com/MariusCiocanel/CwlDemangle)
A translation (line-by-line in many cases) of Swift's Demangler.cpp into Swift.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

## Built With

* [Swift](https://swift.org) - The web framework used
* [Gitlab](https:gitlab.com) - Continuous Integration, Continuous Deployment

### Prerequisites

* Swift: 5.0.1
* Gitlab: 11.10

### Installing

Package Manager

```
.package(url: "https://gitlab.com/thecb4/demanglekit", .exact("0.1.0"))
```

## Using

Parse a String containing a mangled Swift symbol with the parseMangledSwiftSymbol function:

```
let swiftSymbol = try parseMangledSwiftSymbol(input)
```

Print the symbol to a string with description (to get the .default printing options) or use the print(using:) function, e.g.:

```
let result = swiftSymbol.print(using:
   SymbolPrintOptions.default.union(.synthesizeSugarOnTypes))
```

### Break down into end to end tests

Explain what these tests test and why

```
swift test --enable-code-coverage
```

## docs

Docs are available [here](https://thecb4.gitlab.io/demanglekit)

## Roadmap and Contributing

### Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags).



### Roadmap

Please read [ROADMAP](ROADMAP.md) for an outline of how we would like to evolve the library.

### Contributing

Please read [CONTRIBUTING.md](CONTRIBUTING.md) for details on our code of conduct, and the process for changes to us.

### Changes

Please read [CHANGELOG](CHANGELOG.md) for details on changes to the library.


## Authors

* **'Matt Gallagher'** - *Initial work* [mattgallagher](https://cocoawithlove.com)
* **'Marius Ciocanel'** - *Fixed parse crash* - [thecb4](https://github.com/MariusCiocanel)
* **'Cavelle Benjamin'** - *Swift package with docs* - [thecb4](https://thecb4.io)

See also the list of [contributors](https://github.com/your/project/contributors) who participated in this project.

## Acknowledgments

* Hat tip to anyone whose code was used
* Inspiration
* etc
